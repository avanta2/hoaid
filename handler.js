const db = require('./db_connect')

const hoaid_handler = async (event, context, callback) => {
    console.log('## Inside Nodejs .... ')
    db.query('select property_master_id, min_fee, max_fee, hoa_fee, full_address from fee.property_fee_info where property_master_id=35134627')
    .then(res => {
        callback(null,{
            statusCode: 200,
            body: JSON.stringify(res)
        })
    })
    .catch(e => {
        callback(null,{
            statusCode: e.statusCode || 500,
            body: "Could not find Todo: " + e
        })
    })
}

var formatResponse = function(body){
  var response = {
    "statusCode": 200,
    "headers": {
      "Content-Type": "application/json"
    },
    "isBase64Encoded": false,
    "multiValueHeaders": {
      "X-Custom-Header": ["My value", "My other value"],
    },
    "body": body
  }
  return response
}

function getData(query) {
    const result = await db.query(query);
}
module.exports = {hoaid_handler}