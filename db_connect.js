const PgConnection = require('postgresql-easy');
const dbConfig = require('./db_config');
const pg = new PgConnection(dbConfig);
console.log('## DB Connection successful .... ')
module.exports = pg;
