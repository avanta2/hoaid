module.exports =  {
    database: process.env.DATABASE_POSTGRESQL_NAME,
    host: process.env.DATABASE_POSTGRESQL_HOST,
    port: process.env.DATABASE_POSTGRESQL_PORT,
    user: process.env.DATABASE_POSTGRESQL_USERNAME,
    password: process.env.DATABASE_POSTGRESQL_PASSWORD,
}