import sys
import logging
import psycopg2
import json
import os

# rds settings
rds_host  = os.environ['DATABASE_POSTGRESQL_HOST']
rds_username = os.environ['DATABASE_POSTGRESQL_USERNAME']
rds_user_pwd = os.environ['DATABASE_POSTGRESQL_PASSWORD']
rds_db_name = os.environ['DATABASE_POSTGRESQL_NAME']
rds_db_port = os.environ['DATABASE_POSTGRESQL_PORT']

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn_string = "host=%s user=%s password=%s dbname=%s port=%s" % \
                  (rds_host, rds_username, rds_user_pwd, rds_db_name, rds_db_port)

    logger.info("connecting to RDS - " + rds_host + ' and database ' + rds_db_name+ ' and port = ' + rds_db_port)
    conn = psycopg2.connect(conn_string)
except:
    logger.error("ERROR: Could not connect to Postgres instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS Postgres instance succeeded")

def hello(event, context):

    query = """select property_master_id, min_fee, max_fee, hoa_fee, full_address
            from fee.property_fee_info
            where property_master_id=35134627"""

    with conn.cursor() as cur:
        rows = []
        cur.execute(query)
        for row in cur:
            rows.append(row)

    strrows = ','.join(str(v) for v in rows)
    logger.info('DB ROWS: %s' % strrows)

    return { 'statusCode': 200, 'body': strrows}
